@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
            <h3>Nom de l'élément: {{$element->name}}</h3>
            <p>Description brève de cet élément : {{$element->resume}}</p>
            <p>Quantité restante: <strong>{{ $element->quantity }}</strong></p>
            @foreach($categorie as $cat)
                @if ($element->category_id == $cat->id)
                  <button type="button" class="btn btn-secondary">
                    Catégorie : <strong><span class="badge badge-danger ml-2">{{$cat->name}}</span></strong>
                  </button>
                @endif
            @endforeach
            <hr>
            <div class="pull-right">
                <a class="btn btn-danger" href="{{ route('elements.index') }}"><i class="fa fa-reply" aria-hidden="true"> Retour</i></a>
            </div>
        </div>
    </div>
</div>
@endsection