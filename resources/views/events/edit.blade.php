@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header text-danger">
    Modifier cet événement :
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('events.update', $event->id ) }}" >
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label class="text-danger" for="title">Nom de l'événement:</label>
              <input type="text" class="form-control" name="title" value="{{$event->title}}"/>
          </div>
          <div class="form-group">
              <label class="text-danger" for="resume">Description de cet événement:</label>
              <textarea rows="5" columns="5" class="form-control" name="resume">{{$event->resume}}</textarea>
          </div>
          <div class="form-group">
            <label class="text-danger" for="category_id">Catégorie de cet événement :</label>
            <select class="form-control" name="category_id">
            @foreach($categories as $category)
            <option class="text-danger" value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-danger">Modifier</button>
      </form>
  </div>
</div>
@endsection