@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
            <h3>Nom de l'événement: {{$event->title}}</h3>
            <p>Description brève de cet événement : {{$event->resume}}</p>
            @foreach($categorie as $cat)
                @if ($event->category_id == $cat->id)
                  <button type="button" class="btn btn-secondary">
                    Catégorie : <span class="badge badge-danger ml-2">{{$cat->name}}</span>
                  </button>
                @endif
            @endforeach
            <hr>
            <div class="pull-right">
                <a class="btn btn-danger" href="{{ route('events.index') }}"><i class="fa fa-reply" aria-hidden="true"> Retour</i></a>
            </div>
        </div>
    </div>
</div>
@endsection