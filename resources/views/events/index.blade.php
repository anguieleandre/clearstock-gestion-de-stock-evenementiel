@extends('layouts.app')

@section('content')
@auth
    <div class="container mb-3">
        <h2 class="text-danger">Liste des événements :</h2>
        <form action="/categorize" method="post">
            @csrf 
            <div class="form-group">
                <label class="text-danger" for="category_id">Tri par catégorie :</label>
                <select class="form-control text-danger" name="category_id">
                @foreach($categories as $category)
                <option class="text-danger" value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
                </select>
                <button type="submit" class="btn btn-danger mt-2">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                    </svg>
                    Trier</button>

            </div>
        </form>
        <a class="btn btn-danger mt-1" href="{{ route('events.create') }}"><i class="fas fa-plus-circle"> Ajouter un événement</i></a>
    </div>
@endauth
<div class="container">
<div class="table-responsive text-danger">
    <table class="table table-bordered">
      <thead>
        <tr>
            <th class="text-danger" scope="col">#</th>
            <th class="text-danger" scope="col">Nom</th>
            <th class="text-danger" scope="col">Description</th>
            <th class="text-danger" scope="col">Catégorie</th>
            <th class="text-danger" scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($events as $event)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $event->title }}</td>
            <td>{{ Illuminate\Support\Str::limit($event->resume, 35, $end='[...]') }}</td>
            <td>
            @foreach ($categories as $cat)
                @if($event->category_id == $cat->id)
                    <span class="badge badge-danger">{{$cat->name}}</span>
                @endif
            @endforeach
            </td>
            <td>
                <form action="{{ route('events.destroy',$event->id) }}" method="POST" onsubmit="return confirm('Êtes vous certain de vouloir supprimer cet événement ? Cette action est irréversible !')">
    
                    <a class="btn btn-danger" href="{{ route('events.show',$event->id) }}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                        </svg>
                        Voir</a>
                    @auth
                        <a class="btn btn-danger" href="{{ route('events.edit',$event->id) }}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg>
                            Editer</a>
        
                        @csrf
                        @method('DELETE')
        
                        <button type="submit" class="btn btn-danger">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-dash-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path fill-rule="evenodd" d="M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                            Suppr.</button>
                    @endauth
                </form>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
</div>
{{ $events->links() }}
@endsection